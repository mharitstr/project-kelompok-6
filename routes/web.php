<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/coba', function () {
    return view('post.index2');
});
Route::get('/coba2', function () {
    return view('post.index3');
});

Route::get('/master', function(){
    return view('layout.master');
});

Route::group(['middleware' => ['auth']], function () { //login dlu
    Route::resource('profile', 'ProfileController')->only([
        'index', 'update'
    ]);

    Route::resource('post', 'PostController');

    Route::resource('komentar', 'KomentarController')->only([
        'index','store'
    ]);

    //Route::get('like/{id}', 'LikeController@like');
});

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
