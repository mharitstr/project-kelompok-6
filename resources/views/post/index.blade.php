@extends('layout.master')

@section('judul')
Halaman Beranda
@endsection

@section('content')

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-3">
        <a href="/post/create" class="btn btn-primary mb-3">Tambah Post</a>
        
      </div>
      <!-- /.col -->
      <div class="col-md-12">
        
        <div class="card">
          <div class="card-header p-2">
            <ul class="nav nav-pills">
              <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">Activity</a></li>
              <li class="nav-item"><a class="nav-link" href="#timeline" data-toggle="tab">Timeline</a></li>
              <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">Settings</a></li>
            </ul>
          </div><!-- /.card-header -->
          <div class="card-body">
            @forelse ($post as $item)
            <div class="tab-content">
              <div class="active tab-pane" id="activity">
                <!-- Post -->
                <div class="post mb-5">
                  <div class="user-block">
                    <img class="img-circle img-bordered-sm" src="{{asset('admin/dist/img/user1-128x128.jpg')}}" alt="user image">
                    <span class="username">
                      <a href="#">{{$item->user->name}}.</a>
                      <a href="#" class="float-right btn-tool"><i class="fas fa-times"></i></a>
                    </span>
                    <span class="description">Shared publicly - 7:30 PM today</span>
                  </div>

                  <div>                      
                    <h3 class="card-text my-2">{{$item->tulisan}}</h3>
                    <div class="col-6 my-3">
                      <img src="{{asset('gambarFolder/'.$item->gambar)}}" class="card-img-top" alt="...">
                    </div>

                    @if ($item->user->id == Auth::user()->id) <!-- biar yg bisa di edit/delete cuma postingannya sendiri -->
                        
                        <form action="/post/{{$item->id}}" method="POST">
                          @csrf
                          @method('delete')
                          <a href="/post/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                          <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                        </form>
                     
                    @endif
                    
                    
                  </div>

                  <p class="mt-3">
                    <a href="#" class="link-black text-sm mr-2"><i class="fas fa-share mr-1"></i> Share</a>
                    <a href="#" class="link-black text-sm"><i class="far fa-thumbs-up mr-1"></i> Like</a>
                    
                    
                    <span class="float-right">
                      <a href="#" class="link-black text-sm">
                        <i class="far fa-comments mr-1"></i> Comments (0)
                      </a>
                    </span>
                  </p>

                  
                  <!-- Buat nampilin komentar/kritik-->
                    @foreach ($item->komentar as $item)
                      <div class="card">
                        <div class="card-body">
                          <small><b>{{$item->user->name}}</b></small>
                          <p class="card-text">{{$item->isi}}</p>

                        </div>
                      </div>
                    @endforeach

                  <!-- Buat nge komentar/kritik-->
                  <form action="/komentar" method="POST" enctype="multipart/form-data" class="my-3">
                      @csrf

                      <div class="form-group">
                        
                        <input type="hidden" name="post_id" value="{{$item->id}}" id="">
                        <textarea name="isi" class="form-control my-3" placeholder="Type a comment"></textarea>
                        @error('isi')
                          <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        

                        
                      </div>
                      
                      <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                    
                </div>
                <!-- /.post -->
              </div>
              

              
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
            @empty
                Data Berita Belum Ada
            @endforelse


          </div><!-- /.card-body -->
        </div>
        <!-- /.card -->
        
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</section>
<!-- /.content -->

  
@endsection