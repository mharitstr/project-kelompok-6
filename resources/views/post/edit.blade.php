@extends('layout.master')

@section('judul')
Halaman Edit Post
@endsection

@section('content')

<form action="/post/{{$post->id}}" method="post" enctype="multipart/form-data">
    @csrf
    @method('put')

    <div class="form-group">
      <label >Posting</label>
      <textarea name="tulisan" class="form-control" cols="10" rows="5">{{$post->tulisan}}</textarea>
    </div>
    @error('tulisan')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label >Gambar</label>
        <input type="file" name="gambar" class="form-control">
      </div>
      @error('gambar')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror

    <div class="form-group">
        <label>Users</label>
        <input type="hidden" name="user_id" value="{{Auth::user()->id}}" id="">
        <p>{{Auth::user()->name}}</p>
    </div>


    <button type="submit" class="btn btn-primary">Submit</button>
  </form>


@endsection