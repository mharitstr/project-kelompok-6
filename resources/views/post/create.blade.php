@extends('layout.master')

@section('judul')
Halaman Tambah Post
@endsection

@section('content')

<form action="/post" method="post" enctype="multipart/form-data">
    @csrf

    <div class="form-group">
      <label >Posting</label>
      <textarea name="tulisan" class="form-control" cols="10" rows="5"></textarea>
    </div>
    @error('tulisan')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label >Gambar</label>
        <input type="file" name="gambar" class="form-control">
    </div>
    @error('gambar')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

      
      <div class="form-group">
        <label>Users</label>
        <input type="hidden" name="user_id" value="{{Auth::user()->id}}" id="">
                <p>{{Auth::user()->id}}</p>         
      </div> 


    <button type="submit" class="btn btn-primary">Submit</button>
  </form>


@endsection