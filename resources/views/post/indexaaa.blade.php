@extends('layout.master')

@section('judul')
Halaman Beranda
@endsection

@section('content')
@auth
  <a href="/post/create" class="btn btn-primary my-2">Tambah Post</a>
@endauth


<div class="row">
    @forelse ($post as $item)
    <div class="col-4">
        <div class="card">
            <img src="{{asset('gambarFolder/'.$item->gambar)}}" class="card-img-top" alt="...">
            <div class="card-body">
              
              
              <p class="card-text">{{$item->tulisan}}</p>
              

              @auth
              <form action="/post/{{$item->id}}" method="POST">
                @csrf
                @method('delete')
                <a href="/post/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                <a href="/post/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                <input type="submit" value="Delete" class="btn btn-danger btn-sm">
              </form>
              @endauth

              @guest
                <a href="/post/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
              @endguest

            </div>
          </div>
    </div>

    @empty
        Data Berita Belum Ada
    @endforelse
</div>







@endsection