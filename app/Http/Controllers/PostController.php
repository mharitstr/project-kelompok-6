<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Post;
use File;
use App\Like;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post = Post::all();  
        return view('post.index', compact('post'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = DB::table('users')->get();
        return view('post.create',compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'tulisan' => 'required',
            'gambar' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'user_id' => 'required',
        ]);

        $gambarfile = time().'.'.$request->gambar->extension();
        $request->gambar->move(public_path('gambarFolder'), $gambarfile); //folder public/gambar
        
        $post = new Post;

        $post->tulisan = $request->tulisan;
        $post->gambar = $gambarfile;
        $post->user_id = $request->user_id;

        $post->save();

        return redirect('/post');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::findOrFail($id);
        return view('post.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $users = DB::table('users')->get();
        $post = Post::findOrFail($id);
        return view('post.edit', compact('post', 'users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'tulisan' => 'required',
            'gambar' => 'image|mimes:jpeg,png,jpg|max:2048',
            'user_id' => 'required',
        ]);

        $post = Post::find($id);

        //buat yg gambar,if else mau ganti gambar apa egk
        if($request->has('gambar')){
            $gambarfile = time().'.'.$request->gambar->extension();
            $request->gambar->move(public_path('gambarFolder'), $gambarfile); //folder public/gambar
            
            $post->tulisan = $request->tulisan;
            $post->gambar = $gambarfile;
            $post->user_id = $request->user_id;

        } else{           

            $post->tulisan = $request->tulisan;
            $post->user_id = $request->user_id;
        }
        $post->save();
        return redirect('/post');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        $path = "gambarFolder/";
        File::delete($path.$post->gambar);
        $post->delete();

        return redirect('/post');
    }

}
